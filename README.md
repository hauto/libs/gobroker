# GoBroker

Small library to interact with brokers.

**Implemented brokers:**
- RabbitMQ

## Installation

```shell
go get gitlab.com/hauto/libs/gobroker@v0.0.1
```

## How to use

```go
package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/hauto/libs/gobroker"
)

func sampleHandler(ctx context.Context, msg gobroker.Message) error {
	param := ctx.Value("foo").(string)

	fmt.Println("Message received, here is your parameter:", param)
	fmt.Println("Body:", string(msg.GetBody()))

	return nil
}

const (
	sampleQueue = "sample.queue"
)

func main() {
	broker := gobroker.NewRabbitMQBroker()
	broker.AddQueue(sampleQueue).Bind("sample.exchange", "test")

	service := gobroker.NewStreamingService(broker)

	service.Subscribe(sampleQueue, sampleHandler).
		AddParam("foo", "bar")

	err := service.Start("amqp://guest:guest@localhost:5672")
	if err != nil {
		log.Fatal(err)
	}
}
```
