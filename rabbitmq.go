package gobroker

import (
	"context"

	"github.com/rabbitmq/amqp091-go"
)

type RabbitMQBroker struct {
	conn *amqp091.Connection
	ch   *amqp091.Channel

	queues    []*RabbitMQQueue
	exchanges []string

	inShutdown bool
}

func NewRabbitMQBroker() *RabbitMQBroker {
	return &RabbitMQBroker{}
}

func (b *RabbitMQBroker) Connect(url string) error {
	conn, err := amqp091.Dial(url)
	if err != nil {
		return err
	}
	b.conn = conn

	ch, err := b.conn.Channel()
	if err != nil {
		return err
	}
	b.ch = ch

	return nil
}

func (b *RabbitMQBroker) Disconnect() {
	b.conn.Close()
	b.ch.Close()
}

func (b *RabbitMQBroker) Setup() error {
	for _, q := range b.queues {
		_, err := b.ch.QueueDeclare(
			q.name,
			false,
			true,
			false,
			false,
			nil,
		)
		if err != nil {
			return err
		}

		if q.exchange != "" {
			err = b.ch.ExchangeDeclare(
				q.exchange,
				amqp091.ExchangeTopic,
				true,
				false,
				false,
				false,
				nil,
			)
			if err != nil {
				return err
			}
		}

		if q.routingKey != "" {
			err = b.ch.QueueBind(
				q.name,
				q.routingKey,
				q.exchange,
				false,
				nil,
			)
			if err != nil {
				return err
			}
		}
	}

	for _, e := range b.exchanges {
		err := b.ch.ExchangeDeclare(
			e,
			amqp091.ExchangeTopic,
			true,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			return err
		}
	}

	return nil
}

func (b *RabbitMQBroker) Consume(subscription Subscription) error {
	deliveryChan, err := b.ch.Consume(
		subscription.queue,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	return b.handle(subscription, deliveryChan)
}

func (b *RabbitMQBroker) handle(subscription Subscription, deliveryChan <-chan amqp091.Delivery) error {
	for {
		select {
		case delivery := <-deliveryChan:
			msg := RabbitMQMessage{delivery: delivery}
			publisher := RabbitMQPublisher{ch: b.ch}
			err := subscription.handler(subscription.ctx, msg, publisher)
			if err != nil {
				return err
			}
		default:
			if b.inShutdown {
				return nil
			}
		}
	}
}

func (b *RabbitMQBroker) Shutdown() {
	b.inShutdown = true
}

func (b *RabbitMQBroker) AddQueue(name string) *RabbitMQQueue {
	queue := &RabbitMQQueue{
		name: name,
	}

	b.queues = append(b.queues, queue)
	return queue
}

func (b *RabbitMQBroker) AddExchange(name string) {
	b.exchanges = append(b.exchanges, name)
}

type RabbitMQQueue struct {
	name       string
	exchange   string
	routingKey string
}

func (q *RabbitMQQueue) Bind(exchange string, routingKey string) {
	q.exchange = exchange
	q.routingKey = routingKey
}

type RabbitMQMessage struct {
	delivery amqp091.Delivery
}

func (m RabbitMQMessage) GetBody() []byte {
	return m.delivery.Body
}

func (m RabbitMQMessage) GetCorrelationID() string {
	return m.delivery.CorrelationId
}

type RabbitMQPublisher struct {
	ch *amqp091.Channel
}

func (p RabbitMQPublisher) Publish(message []byte, topic string, args map[string]any) error {
	exchange, exists := args["exchange"]
	if !exists {
		exchange = ""
	}

	correlationID, exists := args["correlation_id"]
	if !exists {
		correlationID = ""
	}

	msg := amqp091.Publishing{
		CorrelationId: correlationID.(string),
		Body:          message,
	}

	return p.ch.PublishWithContext(
		context.TODO(),
		exchange.(string),
		topic,
		false,
		false,
		msg,
	)
}
