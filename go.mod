module gitlab.com/hauto/libs/gobroker

go 1.21.1

require github.com/rabbitmq/amqp091-go v1.9.0

require golang.org/x/sync v0.5.0 // indirect
