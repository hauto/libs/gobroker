package gobroker

import (
	"context"
	"errors"
	"log"

	"golang.org/x/sync/errgroup"
)

var (
	BrokerConnectionErr = errors.New("there was an error in the connection")
	BrokerSetupErr      = errors.New("there was an error setting up broker's environment")
)

type Broker interface {
	Connect(url string) error
	Disconnect()
	Setup() error
	Consume(subscription Subscription) error
	Shutdown()
}

func NewStreamingService(broker Broker) *StreamingService {
	return &StreamingService{
		broker: broker,
		ctx:    context.Background(),
	}
}

type StreamingService struct {
	broker        Broker
	subscriptions []*Subscription

	ctx context.Context
}

func (s *StreamingService) Subscribe(queue string, handler HandlerFunc) *Subscription {
	sub := &Subscription{
		queue:   queue,
		handler: handler,
		ctx:     context.TODO(),
	}
	s.subscriptions = append(s.subscriptions, sub)
	return sub
}

func (s *StreamingService) Start(url string) error {
	err := s.broker.Connect(url)
	if err != nil {
		log.Println(err)
		return BrokerConnectionErr
	}
	defer s.broker.Disconnect()

	err = s.broker.Setup()
	if err != nil {
		log.Println(err)
		return BrokerSetupErr
	}

	errs, errGroupCtx := errgroup.WithContext(s.ctx)
	for _, sub := range s.subscriptions {
		func(subscription Subscription) {
			errs.Go(func() error {
				return s.broker.Consume(subscription)
			})
		}(*sub)
	}

	select {
	case <-errGroupCtx.Done():
		return errGroupCtx.Err()
	case <-s.ctx.Done():
		return nil
	}
}

type Message interface {
	GetCorrelationID() string
	GetBody() []byte
}

type Publisher interface {
	Publish(message []byte, topic string, args map[string]any) error
}

type HandlerFunc func(ctx context.Context, msg Message, publisher Publisher) error

type Subscription struct {
	queue   string
	handler HandlerFunc

	ctx context.Context
}

func (s *Subscription) AddParam(key string, value any) *Subscription {
	s.ctx = context.WithValue(s.ctx, key, value)
	return s
}
